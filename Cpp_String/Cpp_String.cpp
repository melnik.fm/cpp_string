﻿#include <iostream>
#include <string>
using namespace std;

int main()
{
	string ST = "String";

	cout << ST << endl;
	cout << "Length: " << ST.length() << endl;
	cout << "First char: " << ST[0] << endl;
	cout << "Last char: " << ST[ST.length() - 1] << endl;

	return 0;
}
